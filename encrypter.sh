#!/bin/bash

echo "  ___   _    _____ __   _____ "
echo " / _ \ | |  |_   _/  | |____ |"
echo "/ /_\ \| |    | | \`| |     / /"
echo "|  _  || |    | |  | |     \ \\"
echo "| | | || |____| | _| |_.___/ /"
echo "\_| |_/\_____/\_/ \___/\____/ "
echo "   https://gitlab.com/alt13   " 
echo "------------------------------"
echo "        FILE ENCRYPTER        "
echo "------------------------------"

menu="Encrypt Decrypt"

select option in $menu; do
	if [ $REPLY = 1 ];
	then
		echo "After encryption original file will be deleted!"
		echo "Enter file name to encrypt:"
		read file;
		gpg -c $file # encrypt
		rm -r $file  # remove file
		echo "The file has been encrypted"
	else
		echo "Enter file name to decrypt:"
		read file2;
		gpg -d $file2 	# decrypt
		echo "The file has been decrypted"
	fi
done
